---
description: "Discover Labo-CBZ's Developments network: Specializing in Docker orchestrator infrastructure for development and validation environments."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ Developments Network Docker Swarm Development Hosting Validation Environment GitOps GitLab Runners Stateless Applications Remote Databases Non-Regression Testing"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Developments Network

The Developments network, operating on VLAN 3104, is dedicated to **Labo-CBZ**'s Docker orchestrator infrastructure, facilitating development hosting across two environments: Development and Validation. This network supports a streamlined gitflow for each project: local development, branch pushes, merges into development for feature testing, validation, and finally, deployment into production. The Develop and Validation environments leverage Docker Swarm services clusters to enable charge tests, clustering behavior tests, and client validation.

![Labo-CBZ Developments Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Developments-4.drawio.svg)

## Development and Validation Environments

The Developments network hosts Docker Swarm clusters for both Development and Validation stages, providing platforms as a service at each deployment. Deployments are automated with GitOps operations, GitLab runners, and are based on testing before merging. All applications are stateless and utilize remote databases outside the orchestrator environment, simulating real development environments found in large projects within major companies. This setup allows developers to validate feature developments and conduct non-regression testing efficiently.

## Developments's List of hosts

* 2 DNS Servers
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)
* 6 Docker servers (VM)

## Services Levels

This netwokr focuses on providing a robust Docker orchestrator infrastructure, facilitating seamless development and validation workflows.

## Service Level: DNS, Reverse Proxy, and Metrics Collection

Similar to all other networks, the Developments network offers essential services including DNS resolution, reverse proxy functionality, and metrics collection. These services ensure secure and efficient operation within the Docker orchestrator infrastructure.

## Service Level: Development Project Hosting in Develop Stage

This service level provides platforms as a service for each deployment in the Development stage, utilizing Docker Swarm. It supports GitOps operations for automated deployment, with GitLab runners facilitating continuous integration and delivery. The environment is designed for developers to validate features and perform non-regression testing on stateless applications, which interact with remote databases.

## Service Level: Development Project Hosting in Validation Stage

In the Validation stage, the Developments network offers a similar environment to the Development stage but is tailored for client testing and validation. This stage allows clients to test and validate every aspect of their product before production deployment. The resources available mimic those of the production environment, enabling comprehensive quality testing and validation by clients.
