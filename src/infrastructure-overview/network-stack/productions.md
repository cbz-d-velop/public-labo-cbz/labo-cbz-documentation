---

description: "Explore Labo-CBZ's high-availability Production network with advanced DNS, proxy, metrics, NFS, and Kubernetes for secure, efficient app deployment."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ, Production Network, High Availability, DNS, Reverse Proxy, Metrics, Docker, Kubernetes, OpenMediaVault, NFS Storage, Secure Application Access"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Production Network

The Production network, operating on VLAN 3107, is specifically designed for **Labo-CBZ**'s critical applications requiring high availability, security, and performance. This network is the foundation of the production environment, hosting Docker servers for containerized applications, OpenMediaVault for NFS shared storage, and a suite of Kubernetes tools for orchestration and management.

![Labo-CBZ Production Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Productions-7.drawio.svg)

## Production Network for Kubernetes Cluster

This network is the backbone of **Labo-CBZ**'s production environment, designed to host critical applications with high availability, security, and performance. This network houses a variety of services and hosts, including Docker servers for containerized applications, OpenMediaVault for NFS shared storage, and a suite of Kubernetes tools for orchestration and management.

## Production's List of Hosts

* 2 DNS Servers
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)
* 9 Docker servers (VM)
* 1 OpenMediaVault Server (VM)

## Services Levels

The Production network provides essential services including DNS resolution, reverse proxy functionality, and metrics collection, tailored for the high availability and performance requirements of the production environment.

## Service Level: DNS Provisioning, Reverse Proxy, and Metrics Collection

The DNS service ensures efficient resolution within the Production network, supporting both local and external queries. The reverse proxy facilitates secure access to application web UIs, while the metric scraper collects and aggregates performance data, offering insights into the health and efficiency of the production environment.

## Service Level: Container Orchestration

Utilizes Rancher K3S for a lightweight, Kubernetes compliant orchestration solution, supporting isolated namespaces, network rules, and secure access via reverse HTTPS tunnel. This setup ensures the seamless operation of containerized applications across the infrastructure. This current website is hosted inside this cluster.

## Service Level: Shared Storage

OpenMediaVault provides NFS shared storage solutions, ensuring data availability and integrity with HA compliance introduced by Proxmox HA services. This service supports stateful applications in the Kubernetes cluster, using NFS for logical volume management and host authentication.

## Service Level: Kubernetes Cluster Administration

Kubernetes cluster administration is streamlined with tools like Portainer, offering a user-friendly interface for managing container deployments, volumes, and network configurations, ensuring efficient operation and maintenance of the production environment.

## Service Level: Continuous Deployment with ArgoCD

ArgoCD have been integrated into the Kubernetes cluster to automate the deployment of applications. It enables declarative GitOps by syncing applications defined in a Git repository with the Kubernetes cluster. This ensures that the state of applications deployed in the cluster matches the configuration files stored in Git. ArgoCD's user-friendly dashboard provides real-time visibility into the status of application deployments, facilitating easy tracking and management of deployment processes. This addition strengthens the infrastructure's CI/CD pipeline, ensuring that updates and new applications are seamlessly and reliably deployed to production environments.
