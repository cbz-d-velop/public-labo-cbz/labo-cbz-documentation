---
description: "Labo-CBZ Services: Hosting, storage, development, DNS, reverse proxy, metrics, phpMyAdmin, Nextcloud, Nexus, PufferPanel, TOR, Sonarqube, Jenkins."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ, Services Network, Hosting, Storage, Development, DNS, Reverse Proxy, Metrics, phpMyAdmin, Nextcloud, Nexus, PufferPanel, TOR, Sonarqube, Jenkins"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Services Network

The Services network, operating on VLAN 3103, is specifically designed for **Labo-CBZ**'s storage, hosting, and development services. This network includes essential services like DNS provisioning, reverse proxy functionality, and metrics collection, alongside specialized services for database management, cloud storage, artefact storage, game server provisioning, CI/CD infrastructure, static code analysis, and TOR proxying.

![Labo-CBZ Services Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Services-3.drawio.svg)

## Services for Enhanced Hosting and Development and other services

The Services network is the backbone of **Labo-CBZ**'s hosting and development capabilities, hosting a variety of services tailored for specific needs such as database management with phpMyAdmin, cloud storage with Nextcloud, artefact storage with Nexus, and more. This setup ensures high availability and seamless operation of services across the organization but provide other services for others purposes not related to the infrastructure of the **Labo-CBZ**.

## Services network's List of hosts

* 2 phpMyAdmin servers (LXC)
* 1 Nextcloud server (LXC)
* 1 Nexus server (LXC)
* 1 PufferPanel server (LXC)
* 1 TOR Proxy/Endpoint server (LXC)
* 1 Sonarqube server (LXC)
* 1 Jenkins server (LXC)
* 2 DNS Servers
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)

## Services levels

The Services network provides a comprehensive suite of services designed to support **Labo-CBZ**'s hosting, storage, and development needs. These services ensure the operational integrity and security of the services infrastructure.

## Service level: DNS Provisioning, reverse proxy and metrics collection

The DNS service guarantees seamless resolution within the Services network, accommodating both internal and external requests. The reverse proxy ensures secure and streamlined access to various service web UIs, enhancing user experience and security. Meanwhile, the metrics scraper plays a crucial role in gathering and synthesizing performance data across services, providing valuable insights into the operational efficiency and reliability of the services ecosystem.

## Service level: SQL Databases tool with phpMyAdmin

The SQL database service, powered by phpMyAdmin, offers a robust platform for managing databases with ease. It supports a wide range of operations, from basic CRUD actions to more complex procedures like database exports and imports, SQL queries execution, and user permissions management. This service is designed to cater to both novice users, with its user-friendly web interface, and experienced developers, who require advanced features for database management. It ensures data integrity and security, providing a reliable foundation for applications that rely on SQL databases.

## Service level: TOR proxying and enable deep web based services

The TOR proxy service extends the capabilities of the Services network into the realm of secure and anonymous communication. It enables the hosting of deep web services and facilitates access to the TOR network, ensuring privacy and security for users. This service is particularly important for sensitive applications that require anonymity or access to the deep web for data retrieval. It incorporates advanced security measures to protect against common vulnerabilities and threats associated with TOR networking, making it a reliable choice for privacy-conscious organizations.

## Service level: Cloud storage and collaboration tool with Nextcloud

Nextcloud represents a pivotal service within the Services network, offering secure and flexible cloud storage solutions. It goes beyond simple file storage, providing features such as file sharing, collaboration tools, and integration with office suites for real-time document editing. Nextcloud's emphasis on security, with end-to-end encryption and robust access controls, ensures that data remains confidential and secure. Its scalability and plugin ecosystem make it an ideal platform for organizations looking to enhance their productivity and collaboration capabilities.

## Service level: Artefact storage with Nexus

The Nexus service is a comprehensive artefact management solution that supports a wide array of package formats and provides a secure repository for software artefacts. It facilitates efficient dependency management, ensuring that builds are reproducible and that artefacts are stored in a controlled environment. Nexus acts as a central hub for artefacts, enabling teams to share and access components easily. Its proxy feature allows for caching of external dependencies, reducing build times and mitigating issues related to external repository downtime.

## Service level: Game server provisioning with PufferPanel

PufferPanel offers a specialized service for game server management and provisioning, catering to the needs of the gaming community within **Labo-CBZ**. It provides an intuitive web interface for deploying, managing, and monitoring game servers, supporting a wide range of games. This service simplifies the process of setting up game servers, making it accessible to users with varying levels of technical expertise. PufferPanel ensures that game servers are running optimally, with tools for resource management and automated updates, enhancing the gaming experience for players.

## Service level: CI/CD infrastructures related pipelines with Jenkins

Jenkins is an integral part of the Services network, providing a robust platform for continuous integration and continuous deployment (CI/CD). It automates the process of code integration, testing, and deployment, facilitating a streamlined development workflow. Jenkins supports a wide range of plugins, allowing for customization and integration with various development tools and services. This service is essential for maintaining high-quality code, enabling teams to detect issues early and deploy updates quickly, thereby increasing the pace of development while ensuring reliability.

## Service level: Static code analysis and compliance with Sonarqube

Sonarqube enhances the development process by offering comprehensive tools for static code analysis. It helps developers identify code smells, bugs, and security vulnerabilities, enforcing coding standards and best practices. Sonarqube integrates seamlessly with CI/CD pipelines, providing automated code quality checks that facilitate the development of high-quality, secure software. Its dashboard offers detailed insights into code quality metrics, making it easier for teams to maintain code health and adhere to compliance requirements.
