---
description: "Explore Labo-CBZ's Runners network: Housing VMs for GitLab and Jenkins runners to automate CI/CD pipelines securely."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ Runners Network GitLab Jenkins CI/CD Pipelines Docker Virtual Machines Secure Automation Development Infrastructure"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Runners Network

The Runners network, operating on VLAN 3105, is tailored for **Labo-CBZ**'s CI/CD automation, hosting 12 VMs dedicated to GitLab and Jenkins runners. These runners, equipped with Docker, automate CI/CD pipelines in a secure and efficient manner, supporting both development and infrastructure tasks.

![Labo-CBZ Runners Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Runners-5.drawio.svg)

## CI/CD Runners Automation Environment

VLAN 3105's Runners network is designed to facilitate seamless CI/CD processes, hosting 6 VMs each for GitLab and Jenkins runners. These runners are capable of creating temporary containers, databases, and environments for development pipelines, as well as executing infrastructure pipelines and scheduled actions, such as custom Docker image builds, SSL/TLS certificate renewal, security scans, and more.

## Runners's List of hosts

* 2 DNS Servers
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)
* 12 Docker servers (VM)

## Services Levels

Focused on CI/CD automation, the network supports GitLab and Jenkins runners for development and infrastructure. GitLab runners handle development pipelines, while Jenkins runners manage infrastructure tasks, ensuring a secure and efficient automation environment.

## Service Level: DNS, Reverse Proxy, and Metrics Collection

The Runners network continues to provide essential services such as DNS resolution, reverse proxy functionality, and metrics collection, ensuring secure and efficient operation within the CI/CD automation infrastructure.

## Service Level: GitLab CI/CD Runners

This service level focuses on development pipelines, offering GitLab runners that automate the creation of temporary containers, databases, and environments necessary for comprehensive development testing and deployment processes.

## Service Level: Jenkins CI/CD Runners

Jenkins runners in the Runners network are dedicated to infrastructure pipelines and scheduled actions. These include tasks like building custom Docker images, renewing SSL/TLS certificates, deploying security scans, and other infrastructure-related automation, ensuring a robust and secure operational environment.
