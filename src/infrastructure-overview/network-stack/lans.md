---
description: "Discover Labo-CBZ's LANS network: A central hub for DNS, reverse proxy, metrics, and secure Reverse HTTPS Tunnel via CloudFlare for robust connectivity."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ LANS Network DNS Provisioning Reverse Proxy Services Network Metrics Collection Reverse HTTPS Tunnel CloudFlare Security Secure Application Access Network Infrastructure"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ LANS Network

The LANS network, residing on VLAN 0 (the default VLAN), serves as the central hub for all endpoint and access point communications across **Labo-CBZ**'s network infrastructure. It is meticulously designed to facilitate seamless inter-network communication, ensuring that each router within this network can communicate with others. This network is pivotal in centralizing the connectivity of **Labo-CBZ**'s extensive network architecture, hosting 7 routers that correspond to the 7 distinct networks beyond the LANS itself. The LANS network is equipped with a DNS providing service, consisting of 2 DNS servers, a reverse proxy, and a metric scraper dedicated to collecting comprehensive metrics data for the network.

![Labo-CBZ LANS Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-DMZ-0.drawio.svg)

## LANS for Centralized Network Management

The LANS network is the cornerstone for centralized network management within **Labo-CBZ**, enabling efficient control and oversight over the interconnectivity and communication between different network segments. This setup ensures that all other networks can operate with a degree of autonomy while still being part of a cohesive whole.

## LANS's List of hosts

* 7 Routers for each of the 7 networks
* 2 DNS Servers
* 1 Reverse Proxy
* 1 Metric Scraper
* 1 Reverse HTTPS tunnel

## Services levels

The LANS network provides a foundational service level that includes DNS resolution, reverse proxy functionality, and metrics collection. These services are critical for maintaining the operational integrity and performance monitoring of **Labo-CBZ**'s network infrastructure. This stack is present in all other network and provide same services but connected.

## Service level: DNS Provisioning, reverse proxy and metrics collection

The DNS service within the LANS network is designed to provide robust DNS resolution capabilities. It includes 2 DNS servers that not only serve the LANS network but also act as secondary resolvers for the local DNS servers in other networks. This design ensures that local DNS modifications within individual networks can be resolved efficiently, with the LANS DNS acting as a fallback resolver.

The reverse proxy component within the LANS network plays a crucial role in managing and directing incoming network traffic to the appropriate destinations. Additionally, the metric scraper is tasked with aggregating and collecting vital metrics data across the network, providing invaluable insights into network performance and health.

## Service level: Reverse HTTPS Tunnel

To facilitate external access to applications within **Labo-CBZ** without requiring a VPN, we employ a reverse HTTPS tunnel. This method allows users without VPN access to reach applications through public DNS and access points. The reverse HTTPS tunnel operates by creating a secure connection from a host within the LANS network to an entry and exit point on CloudFlare servers. This approach eliminates the need for NAT/PAT configurations on routers, ensuring that the entirety of Labo-CBZ's networks and systems remain inaccessible to incoming requests, except through the reverse tunnel. This additional layer of security is bolstered by CloudFlare's robust protection mechanisms.

The tunnel's integration into the LANS network allows for traffic to be precisely redirected through firewalls and routers to specific reverse proxies. This setup ensures secure and controlled access to all protected applications within the Labo-CBZ infrastructure.
