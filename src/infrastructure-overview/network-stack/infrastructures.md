---
description: "Explore Labo-CBZ's Infrastructure network, dedicated to infrastructure services with LDAP, Docker administration, log management, and metrics visualization."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ Infrastructure Network LDAP Docker Administration Log Management Metrics Visualization Portainer Grafana Logstash Kibana ApacheDS Secure Application Access"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Infrastructure Network

The Infrastructure network, operating on VLAN 3102, is specifically designed for **Labo-CBZ**'s infrastructure services, including LDAP for authentication, Docker administration via Portainer, log management with Logstash and Kibana, and metrics visualization through Grafana. This network supports **Labo-CBZ**'s commitment to a secure, efficient, and resilient infrastructure.

![Labo-CBZ Infrastructures Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Infrastructures-2.drawio.svg)

## Infrastructure for Enhanced Service Management

The Infrastructure network is the backbone of **Labo-CBZ**'s service management capabilities, hosting essential services for authentication, Docker administration, log management, and metrics visualization. This setup ensures high availability and seamless operation of infrastructure services across the organization.

## Infrastructure's List of hosts

* 2 DNS Servers
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)
* 1 Portainer server (LXC)
* 1 Grafana server (LXC)
* 2 Logstash servers (LXC)
* 2 Kibana servers (LXC)
* 1 ApacheDS server (LXC)

## Services levels

The **Services levels** section outlines the core services that underpin the **Labo-CBZ** infrastructure, focusing on operational efficiency and security.

## Service level: DNS Provisioning, reverse proxy and metrics collection

The DNS service ensures efficient resolution within the Hypervisors network, supporting both local and external queries. The reverse proxy facilitates secure access to Proxmox web UIs, while the metric scraper collects and aggregates performance data, offering insights into the health and efficiency of the virtualization environment.

The Infrastructure network provides essential services tailored for the management and operation of **Labo-CBZ**'s infrastructure. These services include LDAP for authentication, Docker administration, log management, and metrics visualization, ensuring the operational integrity and security of the infrastructure.

## Service level: LDAP with ApacheDS

The LDAP service, powered by ApacheDS, provides authentication for users and service providers, enabling secure access to web UIs through basic HTTP authentication. This service is crucial for maintaining secure and controlled access to infrastructure services.

## Service level: Docker administration with Portainer

Portainer offers a web UI and agent deployed on all hosts with Docker, facilitating Docker administration, updates, and user management. This service enhances the management and operation of Docker services across the infrastructure.

## Service level: Applicative and System logs reformat with Logstash

Logstash processes all logs from **Labo-CBZ**'s hosts, including application, service, and system logs. This service reformats logs and sends them to the Elasticsearch cluster for indexing and decentralization, supporting security and analysis purposes.

## Service level: Logs visualization with Kibana

Kibana, connected to the Elasticsearch cluster, presents logs in dashboards for analysis. This service enables efficient log visualization and analysis, supporting operational and security insights.

## Service level: Metric and Service visualization with Grafana

Grafana connects to all metric scrapers (Prometheus / node_exporter), providing dashboards for monitoring resources and services across all hosts. This service aids in resource management and service monitoring, ensuring optimal performance and availability.
