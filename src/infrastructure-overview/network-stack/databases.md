---
description: "Explore Labo-CBZ's Databases network: A dedicated environment for hosting a variety of database systems to support development."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ Databases Network SGBD Data Models Development Redis MariaDB Elasticsearch InfluxDB MongoDB Cluster Replication High Availability"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Databases Betwork

VLAN 3106 is **Labo-CBZ**'s dedicated network for all database-related storage, hosting the most commonly used Database Management Systems (DBMS) and data models. This network is designed to provide robust, scalable, and high-availability database services to support the organization's development needs.

![Labo-CBZ Databases Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Databases-6.drawio.svg)

## High Availability and Clustering

All databases within VLAN 3106 are configured for high availability and resilience. SQL databases (MariaDB), document databases (MongoDB), and other specialized databases like Elasticsearch, Redis, and InfluxDB are set up with clustering and replication strategies. Data is validated across two out of three replicas, allowing the system to withstand the loss of up to two nodes in a three-node cluster. For systems where clustering is not possible (InfluxDB, MongoDB), Proxmox HA (High Availability) and Ceph storage are utilized to ensure no single point of failure and high availability compliance.

## Databases's Hosts Configuration

* 2 DNS Servers
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)
* 3 Redis servers (LXC)
* 3 MariaDB servers (LXC)
* 3 Elasticsearch servers (LXC)
* 1 InfluxDB server (LXC)
* 1 MongoDB server (VM)

## Services Levels

VLAN 3106's Databases network is a cornerstone of **Labo-CBZ**'s infrastructure, offering a comprehensive and resilient database ecosystem to support a wide range of development projects and applications. Through its specialized services and high-availability configurations, it ensures reliable data storage and management solutions that meet the evolving needs of developers and applications.

## Service Level: SQL Databases with MariaDB

MariaDB servers are configured for SQL database services, offering robust, scalable, and high-availability solutions for relational data storage and management.

## Service Level: Document Database with MongoDB

MongoDB server provides a high-performance, scalable document database service that supports a wide range of development needs, from simple CRUD operations to complex aggregations and transactions.

## Service Level: Document Indexing with Elasticsearch

Elasticsearch servers offer powerful full-text search, data analytics, and visualization capabilities, making it an essential service for applications requiring complex search features and data analysis.

## Service Level: Decentralized Cache, Key/Value Database with Redis

Redis servers are utilized for high-speed, decentralized caching and as a key/value store, enhancing application performance by reducing data retrieval times.

## Service Level: Timeseries Database with InfluxDB

InfluxDB server specializes in handling timeseries data, ideal for monitoring, real-time analytics, and IoT applications, providing efficient storage and querying of time-stamped data.
