---
description: "Explore Labo-CBZ's Hypervisors network: Dedicated to Proxmox environments with DNS, reverse proxy, metrics, and secure access to web UIs."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ Hypervisors Network DNS Provisioning Reverse Proxy Services Network Metrics Collection Proxmox PVE Proxmox PBS Ceph Storage Secure Application Access"
layout: doc
outline: 2
aside: true
---

# Labo-CBZ Hypervisors Network

The Hypervisors network, operating on VLAN 3101, is specifically designed for **Labo-CBZ**'s Proxmox Virtualization Environment (PVE) and Proxmox Backup Server (PBS). This network facilitates secure and efficient communication between hypervisors, employing three network cards per hypervisor: two for VMs/LXCs and one for local communication and Ceph storage. Positioned in a non-default VLAN for enhanced security and bandwidth stability, this network supports **Labo-CBZ**'s commitment to robust and resilient virtualization infrastructure.

![Labo-CBZ Hypervisors Network](/images/infrastructure-overview/network-stack/Labo-CBZ-v3.0-Hosts_Networks-Hypervisors-1.drawio.svg)

## Hypervisors for Enhanced Virtualization

The Hypervisors network is the backbone of **Labo-CBZ**'s virtualization capabilities, hosting 6 Proxmox PVE nodes for VM and LXC management, and a Proxmox Backup Server (PBS) for reliable data backup and recovery. This setup ensures high availability and seamless operation of virtualized services across the infrastructure.

## Hypervisors's List of hosts

* 2 DNS Servers
* 1 Proxmox PBS (VM)
* 6 Proxmox PVE (Physical)
* 1 Reverse Proxy (LXC)
* 1 Metric Scraper (LXC)

## Services levels

The Hypervisors network provides essential services including DNS resolution, reverse proxy functionality, and metrics collection, similar to the LANS network but tailored for the virtualization environment. These services enable secure remote access to web UIs and ensure the operational integrity of the virtualization infrastructure.

## Service level: DNS Provisioning, reverse proxy and metrics collection

The DNS service ensures efficient resolution within the Hypervisors network, supporting both local and external queries. The reverse proxy facilitates secure access to Proxmox web UIs, while the metric scraper collects and aggregates performance data, offering insights into the health and efficiency of the virtualization environment.

## Service level: Hypervisors

The Proxmox PVE nodes are the core of the Hypervisors network, providing robust virtualization capabilities. These nodes are configured for high availability and optimized for performance, ensuring the seamless operation of VMs and LXCs.

## Service level: Backup Server

The Proxmox Backup Server (PBS) is a critical component of the Hypervisors network, offering reliable backup and recovery solutions for virtualized environments. Hosted on all hosts with HA definition rules and Ceph storage, the PBS ensures that data integrity and availability are maintained, even in the event of a node failure.
