---
description: "Discover Labo-CBZ's OS stack: Proxmox for virtualization, Openmediavault for storage, Ubuntu and Debian for VMs and LXCs, and ipfire for firewall security."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ, OS stack, Proxmox, Openmediavault, Ubuntu, Debian, ipfire, virtualization, storage solutions, VMs, LXCs, firewall security"
layout: doc
outline: 3
aside: true
---

# Operating System Stack

In the realm of **Labo-CBZ,** the choice of Operating Systems (OS) forms the backbone of our infrastructure, enabling us to provide a robust suite of services for developers, including hosting and computational resources. This section delves into the various OS used within our laboratory and the rationale behind their selection, ensuring our team, CBD D-velop, leverages the most efficient and reliable technologies.

![background](/images/infrastructure-overview/operating-system-stack/background.jpg)

## Overwiew

Our Operating System stack is meticulously chosen to cater to the diverse needs of our services. It includes:

* **Proxmox** as our Hypervisor OS, offering a scalable and secure virtualization environment.
* **Openmediavault** for NAS/storage solutions, ensuring reliable data management and accessibility.
* **Ubuntu (server)** as the primary OS for Virtual Machines (VMs), known for its stability and extensive support.
* **Debian** for Linux Containers (LXCs), providing a lightweight and efficient option for running isolated environments.
* **IPFire** as the virtualized firewall OS, securing our network against threats while maintaining optimal performance.
* **Pfsense** as the physical firewall OS, providing advanced security features and robust network protection.

Each of these selections plays a pivotal role in our infrastructure, chosen for their reliability, performance, and compatibility with our development needs.

## Proxmox PVE, for our Hypervisors

Proxmox VE is our chosen Hypervisor OS, facilitating virtualization with a strong emphasis on usability and functionality. It allows us to manage VMs and containers, storage, and networked clusters through a single, integrated platform. The decision to use Proxmox is driven by its open-source nature, robust community support, and its ability to provide a flexible and scalable virtualization solution.

You can find more information about this distribution [here](https://www.proxmox.com/en/proxmox-virtual-environment/overview).

## Proxmox PBS, for our Backups Hypervisors

Proxmox Backup Server (PBS) is our selected Hypervisor OS for backup and disaster recovery purposes. It provides a comprehensive and reliable solution for backing up and restoring virtual machines, containers, and physical hosts. With its intuitive web-based interface and advanced features such as deduplication and encryption, Proxmox PBS ensures the safety and availability of our critical data.

You can find more information about this distribution [here](https://www.proxmox.com/en/proxmox-backup-server/overview).

## Openmediavault, for our NAS/Storage Providers

Openmediavault serves as our NAS and storage solution, offering an easy-to-use, web-based interface that simplifies file sharing, data backup, and storage management. Its selection is based on its open-source framework, extensive plugin system, and compatibility with various hardware and network protocols, making it an ideal choice for our storage needs.

You can found more informations about this distribution [here](https://www.openmediavault.org/).

## Ubuntu (server), for our VMs

Ubuntu is our primary operating system for Virtual Machines. Its widespread adoption, comprehensive documentation, and community support make it an excellent choice for our VMs. Ubuntu's reliability and ease of use ensure that our development environments are stable and efficient, facilitating a seamless development process.

You can found more informations about this distribution [here](https://ubuntu.com/download/server).

## Debian, for our LXcs

For Linux Containers, we rely on Debian 12. Its stability, security, and minimal footprint make it an excellent choice for running multiple isolated environments on a single host. Debian's package management and security features ensure that our containers are secure, up-to-date, and running efficiently.

You can found more informations about this distribution [here](https://www.debian.org/releases/index.fr.html).

## IPfire,for our Virtualized Firewalls

ipfire is our chosen firewall OS, providing a robust security layer for our network. It is selected for its flexibility, ease of configuration, and comprehensive security features. ipfire allows us to maintain a secure and efficient network environment, protecting our resources and services from external threats.

You can found more informations about this distribution [here](https://www.ipfire.org/).

## Pfsense, for our Physical Firewalls

Pfsense is our selected physical firewall OS, providing advanced security features and robust network protection. It offers a user-friendly web interface for easy configuration and management of firewall rules, VPN connections, and traffic shaping. pfsense ensures the integrity and confidentiality of our network, safeguarding our infrastructure from unauthorized access and potential threats.

You can found more informations about this distribution [here](https://www.pfsense.org/).
