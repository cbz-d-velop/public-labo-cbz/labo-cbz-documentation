---
description: "Overview of Labo-CBZ's infrastructure, focusing on the hypervisor stack, network layout, dual-NIC setup for VMs/LXCs, VLAN switches for segmentation, and Ceph storage integration."
head:
  - - meta
    - name: keywords
      content: "Labo-CBZ Legal Mentions, Data Protection, Intellectual Property, Web Hosting, Company Information, GDPR Compliance"
layout: doc
outline: 3
aside: true
---

# Legal Mentions

Discover **Labo-CBZ**'s commitment to transparency and legal compliance. From safeguarding personal data to respecting intellectual property rights and providing clear web hosting information, we adhere to the highest standards. This section outlines our practices and policies, ensuring users and partners are well-informed about our legal obligations and their rights.

![background](/images/legal-mentions/background.jpg)

## Compagny related mentions

| Host Name | Labo-CBZ / CBZ D-velop |
|-|-|
| Company Name | [Robin CROMBEZ / sole proprietorship](https://www.societe.com/societe/monsieur-robin-crombez-884652587.html) |
| Contact Email Address | [webmaster@labo-cbz.net](mail:webmaster@labo-cbz.net) |
| Website | [www.labo-cbz.net](https://www.labo-cbz.net/) |
| Phone Number | [06.02.36.37.28](tel:06.02.36.37.28) |

| Company Information | Labo-CBZ / CBZ D-velop |
|-|-|
| SIREN | 884652587 |
| SIRET | 88465258700012 |
| VAT | FR40884652587 |

## Web hosting related mentions

*This website is produced and hosted by the same compagny.*

## Personal Data Usage Mentions

This site guarantees the protection of your personal data. No login cookies are used, and no tracking is performed. We do not use any user analysis frameworks, tracking, or SEO. No data is stored on our servers, and no data is collected. Furthermore, the site is independent of social networks, and no information about user behavior is recorded.

## Intellectual Property References

In this section, you will find the references used throughout this site for its design, development, deployment, and visual sources.

* [VitePress](https://vitepress.dev/),Vite & Vue Powered Static Site Generator.
* [Unsplash](https://unsplash.com/fr), the source for visuals on the Internet.
* [Kubernetes](https://kubernetes.io/fr/), the professional container orchestration solution.
* [Docker Swarm mode](https://docs.docker.com/engine/swarm/), an advanced feature for managing a cluster of Docker daemons.
* [DNS Cloudflare](https://www.cloudflare.com/fr-fr/application-services/products/dns/), a professional-grade reference DNS with one of the fastest response times in the market.

## Legal References

In this section, you will find the references used throughout this site for its writing and legality.

* [Mentions on your website: obligations to comply with](https://www.economie.gouv.fr/entreprises/site-internet-mentions-obligatoires)
* [The General Data Protection Regulation (GDPR), mode of operation](https://www.economie.gouv.fr/entreprises/site-internet-mentions-obligatoires)
