---
description: Explore Labo-CBZ's tech journey from HP servers to Proxmox & Ceph integration. Discover innovations shaping infrastructure excellence.
head:
  - - meta
    - name: keywords
      content: Labo-CBZ infrastructure management Proxmox Ceph storage virtualization networking hardware upgrades technological advancements Debian 10 VLAN segmentation network security Ansible automation scalability reliability
layout: doc
outline: 3
aside: true
---

# Global Project Roadmap

Embark on **Labo-CBZ**'s technological odyssey, a journey marked by strategic milestones and technological innovations. From humble beginnings to a future of endless possibilities, our roadmap is a testament to our commitment to excellence and innovation.

![background](/images/project-roadmap/background.jpg)

## Timeline of Key Milestones

### 2019: The Foundation

- **Initial Setup and Installation**: The journey began with a foundational HP Proliant server, marking our first foray into server hardware and operating systems.

### 2020: Expansion and Exploration

- **New Server Additions**: Embracing **Debian 10** for its stability and security, **Labo-CBZ** expanded its infrastructure.
- **Virtualization Testing with VirtualBox**: A pivotal year for testing and validating virtualization concepts, setting the stage for more sophisticated solutions.

### 2021: Advanced Virtualization and Storage

- **Proxmox VE Integration**: Transitioning to Proxmox VE for enhanced virtualization capabilities.
- **Ceph Storage Implementation**: Adopting Ceph storage solutions for scalable and reliable data management.

### 2022 and Beyond: Looking Forward

- **Future Technologies**: Exploring cutting-edge technologies like Kubernetes, Docker, and advanced networking solutions.
- **Infrastructure Excellence**: Continuous improvement in our infrastructure for scalability, reliability, and security.

## Vritualization versions, after 2022

### Labo-CB Version 1.0 / <Badge type="warning" text="2022" />

The debut of **Labo-CB Version 1.0** marked a significant milestone in our journey towards efficient virtualization. With the deployment of **Proxmox** hypervisors and the implementation of Linux Containers (LXC), this version introduced a lightweight virtualization technology that facilitated efficient resource utilization and rapid application deployment within **Labo-CBZ**'s environment. The focus was on creating a scalable, flexible infrastructure that could support the growing demands of our projects.

### Labo-CB Version 2.0 / <Badge type="warning" text="2023" />

**Labo-CB Version 2.0** brought about a paradigm shift in **Labo-CBZ**'s architecture, with a strong emphasis on network segmentation and enhanced security. By leveraging VLANs (Virtual Local Area Networks), this version enabled logical partitioning of network resources, facilitating secure communication between different components of our infrastructure while effectively mitigating potential security risks. This approach not only bolstered our security posture but also introduced a more sophisticated network management strategy.

### Labo-CB Version 3.0 / <Badge type="tip" text="2024" />

Looking ahead, **Labo-CB Version 3.0** aims to further revolutionize our virtualization landscape. While details are still under wraps, we are exploring cutting-edge technologies that promise to enhance our infrastructure's efficiency, security, and scalability. Expect innovations in automation, more advanced containerization solutions, and further advancements in network security. Stay tuned as we continue to push the boundaries of what's possible in virtualization technology.

Stay connected for more updates on **Labo-CB Version 3.0** as we chart our course towards a more innovative and secure future.
