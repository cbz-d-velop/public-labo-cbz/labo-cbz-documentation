# ADR Template: Changes to Architecture and Behavior

* Status (Proposed | Accepted | Rejected | Deprecated | Superseded)

## Context

Briefly describe the context and background of the change, including any relevant past decisions, events, or circumstances that have led to this proposal.

## Decision

State the proposed change, including any benefits and drawbacks, and explain why this decision is the best option.

## Consequences

Explain the potential consequences of the proposed change, including any risks, impacts, or dependencies that should be considered.

## Alternatives

List any alternative options or proposals that were considered and why they were rejected or not chosen.

## References

List any relevant sources, links, or materials that were used to inform or support this proposal.
Don't forget to update the Source page file if any References are added here.
