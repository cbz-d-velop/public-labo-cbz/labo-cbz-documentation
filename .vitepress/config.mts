import { defineConfig } from "vitepress"

// https://vitepress.dev/reference/site-config
export default defineConfig({
  lang: "en-US",
  title: "Labo-CBZ Documentation",
  description: "All the public documentation about Labo-CBZ.",
  srcDir: "./src",
  outDir: "./dist",
  sitemap: {
    hostname: "http://localhost"
  },
  head: [
    ['link', { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  ],
  base: "/",
  lastUpdated: true,
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    logo: "/images/logo/logo-right.png",
    siteTitle:"Labo-CBZ Documentation",
    nav: [
      { text: "Home", link: "/", activeMatch: "/" },
      { text: "About Labo-CBZ", link: "/about-labo-cbz/", activeMatch: "/about-labo-cbz/" },
      { text: "Project Roadmap", link: "/project-roadmap/", activeMatch: "/project-roadmap/"},
      {
        text: "Infrastructure Overview",
        items: [
          { text: "Hypervisor Stack", link: "/infrastructure-overview/hypervisor-stack/" , activeMatch: "/infrastructure-overview/hypervisor-stack/" },
          { text: "Storage Stack", link: "/infrastructure-overview/storage-stack/" , activeMatch: "/infrastructure-overview/storage-stack/" },
          { text: "Network Stack", link: "/infrastructure-overview/network-stack/" , activeMatch: "/infrastructure-overview/network-stack/" },
          { text: "Operating System Stack", link: "/infrastructure-overview/operating-system-stack/" , activeMatch: "/infrastructure-overview/operating-system-stack/" },
          { text: "Service Provisioning", link: "/infrastructure-overview/service-provisioning/" , activeMatch: "/infrastructure-overview/service-provisioning/" }
        ],
        activeMatch: "/infrastructure-overview/",
      },
      { text: "ADRs", link: "/adrs/", activeMatch: "/adrs/"},
      { text: "Legal Mentions", link: "/legal-mentions/", activeMatch: "/legal-mentions/"},
    ],

    sidebar: {
        "/infrastructure-overview/network-stack/": [
          { text: "Overwiew", link: "/infrastructure-overview/network-stack/" },
          {
            text: "Network Stack",
            items: [
              { text: "LANS", link: "/infrastructure-overview/network-stack/lans" },
              { text: "Hypervisors", link: "/infrastructure-overview/network-stack/hypervisors" },
              { text: "Infrastructures", link: "/infrastructure-overview/network-stack/infrastructures" },
              { text: "Services", link: "/infrastructure-overview/network-stack/services" },
              { text: "Developments", link: "/infrastructure-overview/network-stack/developments" },
              { text: "Runners", link: "/infrastructure-overview/network-stack/runners" },
              { text: "Databases", link: "/infrastructure-overview/network-stack/databases" },
              { text: "Productions", link: "/infrastructure-overview/network-stack/productions" },
            ]
          },
        ],
        "/infrastructure-overview/service-provisioning/": [
          { text: "Overwiew", link: "/infrastructure-overview/service-provisioning/" },
          {
            text: "Virtualization",
            collapsed: false,
            items: [
              { text: "Virtual Machines", link: "/infrastructure-overview/service-provisioning/virtualization/"},
              { text: "Linux Containers", link: "/infrastructure-overview/service-provisioning/virtualization/"},
              { text: "Backups Solutions", link: "/infrastructure-overview/service-provisioning/virtualization/"},
              { text: "High availability", link: "/infrastructure-overview/service-provisioning/virtualization/"},
              { text: "Monitoring", link: "/infrastructure-overview/service-provisioning/virtualization/"},]
          },
          {
            text: "Data Storages",
            collapsed: false,
            items: [
              { text: "Relational databases", link: "/infrastructure-overview/service-provisioning/data-storages/"},
              { text: "Documents databases", link: "/infrastructure-overview/service-provisioning/data-storages/"},
              { text: "Key/Value databases", link: "/infrastructure-overview/service-provisioning/data-storages/"},
              { text: "Indexation databases", link: "/infrastructure-overview/service-provisioning/data-storages/"},
              { text: "Timeseries databases", link: "/infrastructure-overview/service-provisioning/data-storages/"},
            ]
          },
          {
            text: "Development Services",
            collapsed: false,
            items: [
              { text: "Quality standards", link: "/infrastructure-overview/service-provisioning/development-services/"},
              { text: "Artifacts storages", link: "/infrastructure-overview/service-provisioning/development-services/"},
            ]
          },
          {
            text: "Plateform As A Service",
            collapsed: false,
            items: [
              { text: "Development plateforms", link: "/infrastructure-overview/service-provisioning/plateform-as-a-service/"},
              { text: "Validation plateforms", link: "/infrastructure-overview/service-provisioning/plateform-as-a-service/"},
              { text: "Production plateforms", link: "/infrastructure-overview/service-provisioning/plateform-as-a-service/"},
            ]
          },
          {
            text: "CICD",
            collapsed: false,
            items: [
              { text: "GitLab pipelines", link: "/infrastructure-overview/service-provisioning/cicd/"},
              { text: "Jenkins pipelines", link: "/infrastructure-overview/service-provisioning/cicd/"},
              { text: "Docker images", link: "/infrastructure-overview/service-provisioning/cicd/"},
            ]
          },
          {
            text: "Remote access",
            collapsed: false,
            items: [
              { text: "Reverses HTTPS tunnels", link: "/infrastructure-overview/service-provisioning/remote-access/"},
              { text: "Reverses proxies", link: "/infrastructure-overview/service-provisioning/remote-access/"},
              { text: "Cluster DNS cascades", link: "/infrastructure-overview/service-provisioning/remote-access/"},
              { text: "WAF securities", link: "/infrastructure-overview/service-provisioning/remote-access/"},
              { text: "Crypted VPN", link: "/infrastructure-overview/service-provisioning/remote-access/"},
              { text: "LDAP authentication", link: "/infrastructure-overview/service-provisioning/remote-access/"}
            ]
          },
        ],
        "/ADRs/": [
          {
            text: "Virtualization Infrastructure",
            collapsed: false,
            items: [
              { text: "Markdown Examples", link: "/1" },
            ]
          },
          {
            text: "Service Infrastructure",
            collapsed: false,
            items: [
              { text: "Markdown Examples", link: "/1" },
            ]
          },
          {
            text: "Service Provisioning",
            collapsed: false,
            items: [
              { text: "Markdown Examples", link: "/1" },
            ]
          },
        ],
      },
    socialLinks: [
      { icon: {
        svg: '<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 380 380"><defs><style>.cls-1{fill:#e24329;}.cls-2{fill:#fc6d26;}.cls-3{fill:#fca326;}</style></defs><g id="LOGO"><path class="cls-1" d="M282.83,170.73l-.27-.69-26.14-68.22a6.81,6.81,0,0,0-2.69-3.24,7,7,0,0,0-8,.43,7,7,0,0,0-2.32,3.52l-17.65,54H154.29l-17.65-54A6.86,6.86,0,0,0,134.32,99a7,7,0,0,0-8-.43,6.87,6.87,0,0,0-2.69,3.24L97.44,170l-.26.69a48.54,48.54,0,0,0,16.1,56.1l.09.07.24.17,39.82,29.82,19.7,14.91,12,9.06a8.07,8.07,0,0,0,9.76,0l12-9.06,19.7-14.91,40.06-30,.1-.08A48.56,48.56,0,0,0,282.83,170.73Z"/><path class="cls-2" d="M282.83,170.73l-.27-.69a88.3,88.3,0,0,0-35.15,15.8L190,229.25c19.55,14.79,36.57,27.64,36.57,27.64l40.06-30,.1-.08A48.56,48.56,0,0,0,282.83,170.73Z"/><path class="cls-3" d="M153.43,256.89l19.7,14.91,12,9.06a8.07,8.07,0,0,0,9.76,0l12-9.06,19.7-14.91S209.55,244,190,229.25C170.45,244,153.43,256.89,153.43,256.89Z"/><path class="cls-2" d="M132.58,185.84A88.19,88.19,0,0,0,97.44,170l-.26.69a48.54,48.54,0,0,0,16.1,56.1l.09.07.24.17,39.82,29.82s17-12.85,36.57-27.64Z"/></g></svg>'
      }, link: "https://gitlab.com/cbz-d-velop" },
      { icon: "github", link: "https://github.com/CBZ-D-velop" },
    ],

    search: {
      provider: "local"
    },

    footer: {
      message: "Released under the MIT License.",
      copyright: "Copyright © Robin CROMBEZ - Labo-CBZ 2024",
    },
  }
})
